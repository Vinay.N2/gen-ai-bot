import Bot from "../../Assets/bot.png";
import "./Navbar.scss";
const Navbar = () => {
  return (
    <div>
      <nav className="navbar">
        <div className="navbar-logo">
          <img src={Bot} alt="Bot image" width="40px" />
          <a className="genai-navabar-brand" href="#">
            TITAN
          </a>
        </div>
      </nav>
      <div className="spacer"></div>
    </div>
  );
};

export default Navbar;
