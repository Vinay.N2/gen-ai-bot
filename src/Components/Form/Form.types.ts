export interface Message {
  content: string;
  sender: string;
  loading: boolean;
}

export const defaultMessage: Message = {
  content: "Hello! How can I help you today ?",
  sender: "Bot",
  loading: false,
};

export const getRequestBody = (messageContent: string) => {
  return {
    query: messageContent,
    page_size: "1",
    offset: 0,
    spellCorrectionSpec: {
      mode: "AUTO",
    },
    queryExpansionSpec: {
      condition: "AUTO",
    },
    contentSearchSpec: {
      summarySpec: {
        summaryResultCount: 5,
      },
      snippetSpec: {
        maxSnippetCount: 1,
      },
    },
  };
};

export function delay(ms: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
