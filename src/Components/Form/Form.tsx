import { ChangeEvent, useState, useEffect, useRef } from "react";
import SendRoundedIcon from "@mui/icons-material/SendRounded";
import Bot from "../../Assets/bot.png";
import LoadingGif from "../../Assets/loading.gif";
import "./Form.scss";
import axios from "axios";
import { Message, defaultMessage, getRequestBody } from "./Form.types";

const Form = () => {
  const [message, setMessage] = useState<string>("");
  const [messages, setMessages] = useState<Array<Message>>([defaultMessage]);
  const [loading, setLoading] = useState(false);
  const messagesContainerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    console.log("from useEffect", messages);
    scrollToBottom();
  }, [messages]);

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setMessage(event.target.value);
  };

  const scrollToBottom = () => {
    if (messagesContainerRef.current) {
      const { scrollHeight, clientHeight } = messagesContainerRef.current;
      const scrollOffset = scrollHeight - clientHeight + 500; // Add desired offset value
      messagesContainerRef.current.scrollTop = scrollOffset;
    }
  };

  const handleSendMessage = async (
    event: ChangeEvent<HTMLFormElement>
  ): Promise<void> => {
    event.preventDefault();
    const apiUrl = import.meta.env.VITE_GEN_API_URL;
    const messageContent = message.trim();
    const bearerToken = localStorage.getItem("TOKEN");
    if (messageContent !== "") {
      let newMessage: Message = {
        content: message,
        sender: "Me", // Change this to the appropriate sender information
        loading: false,
      };
      setMessages((prevMessages) => {
        const updatedMessages = [...prevMessages, newMessage];
        return updatedMessages;
      });
      setMessage("");
      setLoading(true);
      try {
        const response = await axios.post(
          apiUrl,
          getRequestBody(messageContent),
          {
            headers: {
              authorization: `Bearer ${bearerToken}`,
              "content-type": "application/json",
            },
          }
        );
        const result = response?.data;
        newMessage = {
          content: `${result?.summary?.summaryText}`,
          sender: "Bot",
          loading: false,
        };
        setMessages((prevMessages) => {
          const updatedMessages = [...prevMessages, newMessage];
          return updatedMessages;
        });
        setLoading(false);
        scrollToBottom();
      } catch (error) {
        console.log(error);
        newMessage = {
          content: `Sorry, Something went Wrong, please try again after sometime`,
          sender: "Bot",
          loading: false,
        };
        setMessages((prevMessages) => {
          const updatedMessages = [...prevMessages, newMessage];
          return updatedMessages;
        });
        setLoading(false);
      }
    }
  };
  return (
    <div className="chat-wrapper">
      <div className="messages-container" ref={messagesContainerRef}>
        {messages.map((msg, index) => (
          <div
            key={index}
            className={`message ${msg.sender === "Me" ? "sent" : "received"}`}
          >
            <div className="sender">
              {msg.sender === "Me" ? (
                "GU"
              ) : (
                <img className="sender-avatar" src={Bot} alt="bot" />
              )}
            </div>
            <div className="content">
              {msg?.loading ? "Loading..." : msg?.content}
            </div>
          </div>
        ))}
        {loading ? (
          <div className={`message received`}>
            <div className="sender">
              <img className="sender-avatar" src={Bot} alt="bot" />
            </div>
            <div className="content">
              <img src={LoadingGif} alt="Loading Gif" width="36px" />
            </div>
          </div>
        ) : (
          ""
        )}
        <div className="spacer"></div>
      </div>
      <form onSubmit={handleSendMessage}>
        <div className="input-container">
          <input
            type="text"
            value={message}
            onChange={handleInputChange}
            placeholder="Send a Message"
          />
          <button
            type="submit"
            className="submit-btn"
            disabled={message === ""}
          >
            <SendRoundedIcon />
          </button>
        </div>
      </form>
    </div>
  );
};

export default Form;
